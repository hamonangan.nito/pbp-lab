from django.contrib.auth.models import User
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.

class Lab3UnitTest(TestCase):

    def test_index_is_exist(self):
        try:
            admin = User.objects.get(username="admin")
        except:
            return None
        response = Client().force_login(admin).get('/lab-3/')
        self.assertEqual(response.status_code, 200) 

    def test_index_func(self):
        found = resolve('/lab-3/')
        self.assertEqual(found.func, index)

    def test_add_page_is_exist(self):
        try:
            admin = User.objects.get(username="admin")
        except:
            return None
        response = Client().force_login(admin).get('/lab-2/add')
        self.assertEqual(response.status_code, 200)

    def test_post_friend(self):
        try:
            admin = User.objects.get(username="admin")
        except:
            return None
        response = Client().force_login(admin).post('/lab-3/add', 
            {'name':'dummy', 'npm':'99', 'dob':'2021-10-04'})
        self.assertEqual(response.status_code, 201)
        



    