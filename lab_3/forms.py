from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
    
    error_messages = {
        'required': 'Please type this form'
    }

    name_attrs = {
        'type': 'text',
        'placeholder': 'ketik nama'
    }

    npm_attrs = {
        'type': 'text',
        'placeholder': 'ketik NPM'
    }

    dob_attrs = {
        'type': 'date',
    }

    name = forms.CharField(label='Nama:', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    npm = forms.IntegerField(label='NPM:', required=True, widget=forms.NumberInput(attrs=npm_attrs))
    dob = forms.DateField(label='Tanggal lahir:', required=True, widget=forms.DateInput(attrs=dob_attrs))

