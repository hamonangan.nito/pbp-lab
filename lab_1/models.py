from django.db import models
from datetime import date

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here

class Friend(models.Model):
    name = models.CharField(max_length=30)
    # TODO Implement missing attributes in Friend model
    npm = models.IntegerField(default=0)
    dob = models.DateField(("Date of birth"), default=date.today)

    def __str__(self):
        return self.name