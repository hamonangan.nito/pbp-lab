Lab 2 Answer - Bornyto Hamonangan (2006486084)
---
### Q1: Apakah perbedaan antara JSON dan XML?
Perbedaan JSON dan XML adalah JSON merupakan meta language, sedangkan XML adalah markup language.
Secara kasat mata, berkas JSON lebih sederhana dan ringan.
Fungsi dari kedua berkas ini identik, yaitu menyimpan data objek.

### Q2: Apakah perbedaan antara HTML dan XML?
Kali ini keduanya sama-sama bahasa markup dan memiliki sintaks yang sama.
Perbedaannya terletak pada fungsi, HTML digunakan untuk membuat kerangka halaman.
Di sisi lain, XML digunakan untuk menyimpan data objek.