from django.contrib.auth.models import User
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_note, note_list
from django.http import HttpRequest
# Create your tests here.

class Lab4UnitTest(TestCase):

    def test_index_is_exist(self):
        try:
            admin = User.objects.get(username="admin")
        except:
            return None
        response = Client().force_login(admin).get('/lab-4/')
        self.assertEqual(response.status_code, 200)

    def test_index_func(self):
        found = resolve('/lab-4/')
        self.assertEqual(found.func, index)

    def test_add_note_is_exist(self):
        try:
            admin = User.objects.get(username="admin")
        except:
            return None
        response = Client().force_login(admin).get('/lab-4/add')
        self.assertEqual(response.status_code, 200)

    def test_add_note_func(self):
        found = resolve('/lab-4/add')
        self.assertEqual(found.func, add_note)

    def test_note_list_is_exist(self):
        try:
            admin = User.objects.get(username="admin")
        except:
            return None
        response = Client().force_login(admin).get('/lab-4/card')
        self.assertEqual(response.status_code, 200)
    
    def test_note_list_func(self):
        found = resolve('/lab-4/card')
        self.assertEqual(found.func, note_list)

    def test_post_note(self):
        try:
            admin = User.objects.get(username="admin")
        except:
            return None
        response = Client().force_login(admin).post('/lab-4/add', 
            {'msg_from':'mario', 'msg_to':'luigi', 'title':'dummy', 'message':'i love coins'})
        self.assertEqual(response.status_code, 201)