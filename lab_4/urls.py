from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add', add_note, name='add'),
    path('card', note_list, name='card')
]