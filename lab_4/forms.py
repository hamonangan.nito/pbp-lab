from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['msg_from', 'msg_to', 'title', 'message']
    
    error_messages = {
        'required': 'Please type this form'
    }

    msg_from_attrs = {
        'type': 'text',
    }

    msg_to_attrs = {
        'type': 'text',
    }

    title_attrs = {
        'type': 'text',
    }

    message_attrs = {
        'type': 'text',
        'placeholder': 'Type your message here...'
    }

    msg_from = forms.CharField(label='From:', required=True, max_length=30, widget=forms.TextInput(attrs=msg_from_attrs))
    msg_to = forms.CharField(label='To:', required=True, max_length=30, widget=forms.TextInput(attrs=msg_to_attrs))
    title = forms.CharField(label='Title:', required=True, max_length=50, widget=forms.TextInput(attrs=title_attrs))
    message = forms.CharField(label='Message:', required=True, max_length=30, widget=forms.Textarea(attrs=message_attrs))

