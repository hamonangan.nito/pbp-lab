from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from lab_4.forms import NoteForm
from lab_2.models import Note
# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    form = NoteForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4')

    response = {'form': form }
    return render(request, 'lab4_form.html', response)

@login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)