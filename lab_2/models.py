from django.db import models

# Create your models here.
class Note(models.Model):
    msg_to = models.CharField(max_length=30)
    msg_from = models.CharField(max_length=30)
    title = models.CharField(max_length=50)
    message = models.TextField(default="Type your message here...")

    def __str__(self):
        return self.title + "  to: " + self.msg_to
