from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, xml, json
from django.http import HttpRequest
# Create your tests here.

class Lab2UnitTest(TestCase):

    def test_index_is_exist(self):
        response = Client().get('/lab-2/')
        self.assertEqual(response.status_code, 200)

    def test_index_func(self):
        found = resolve('/lab-2/')
        self.assertEqual(found.func, index)

    def test_json_is_exist(self):
        response = Client().get('/lab-2/json')
        self.assertEqual(response.status_code, 200)

    def test_json_func(self):
        found = resolve('/lab-2/json')
        self.assertEqual(found.func, json)

    def test_xml_is_exist(self):
        response = Client().get('/lab-2/xml')
        self.assertEqual(response.status_code, 200)
    
    def test_xml_func(self):
        found = resolve('/lab-2/xml')
        self.assertEqual(found.func, xml)


    